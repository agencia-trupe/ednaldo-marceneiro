-- MySQL dump 10.13  Distrib 5.7.10, for Linux (x86_64)
--
-- Host: localhost    Database: ednaldo-marceneiro
-- ------------------------------------------------------
-- Server version	5.7.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `banners`
--

DROP TABLE IF EXISTS `banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subtitulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `frase` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banners`
--

LOCK TABLES `banners` WRITE;
/*!40000 ALTER TABLE `banners` DISABLE KEYS */;
INSERT INTO `banners` VALUES (1,1,'ARMÁRIOS DE COZINHA','Marcenaria sob medida','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed congue placerat varius.','pexels-photo-128639_20160902181049.jpeg','2016-09-02 18:10:50','2016-09-02 18:10:50'),(2,2,'LOREM IPSUM DOLOR','Armários para cozinhas','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed congue placerat varius.','pexels-photo_20160902181124.jpg','2016-09-02 18:11:25','2016-09-02 18:11:46');
/*!40000 ALTER TABLE `banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contato`
--

DROP TABLE IF EXISTS `contato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contato` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone_operadora` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `whatsapp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `whatsapp_operadora` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contato`
--

LOCK TABLES `contato` WRITE;
/*!40000 ALTER TABLE `contato` DISABLE KEYS */;
INSERT INTO `contato` VALUES (1,'contato@trupe.net','11 96931·7310','Claro','11 94317·3770','Vivo','#',NULL,'2016-09-02 18:02:53');
/*!40000 ALTER TABLE `contato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contatos_recebidos`
--

DROP TABLE IF EXISTS `contatos_recebidos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contatos_recebidos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contatos_recebidos`
--

LOCK TABLES `contatos_recebidos` WRITE;
/*!40000 ALTER TABLE `contatos_recebidos` DISABLE KEYS */;
/*!40000 ALTER TABLE `contatos_recebidos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `galeria_categoria_imagem`
--

DROP TABLE IF EXISTS `galeria_categoria_imagem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galeria_categoria_imagem` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `categoria_id` int(10) unsigned NOT NULL,
  `imagem_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `galeria_categoria_imagem_categoria_id_index` (`categoria_id`),
  KEY `galeria_categoria_imagem_imagem_id_index` (`imagem_id`),
  CONSTRAINT `galeria_categoria_imagem_categoria_id_foreign` FOREIGN KEY (`categoria_id`) REFERENCES `galeria_categorias` (`id`) ON DELETE CASCADE,
  CONSTRAINT `galeria_categoria_imagem_imagem_id_foreign` FOREIGN KEY (`imagem_id`) REFERENCES `galeria_imagens` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `galeria_categoria_imagem`
--

LOCK TABLES `galeria_categoria_imagem` WRITE;
/*!40000 ALTER TABLE `galeria_categoria_imagem` DISABLE KEYS */;
INSERT INTO `galeria_categoria_imagem` VALUES (1,1,1,NULL,NULL),(2,1,2,NULL,NULL),(3,1,3,NULL,NULL),(4,1,4,NULL,NULL),(5,1,5,NULL,NULL),(6,1,6,NULL,NULL);
/*!40000 ALTER TABLE `galeria_categoria_imagem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `galeria_categorias`
--

DROP TABLE IF EXISTS `galeria_categorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galeria_categorias` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `galeria_categorias`
--

LOCK TABLES `galeria_categorias` WRITE;
/*!40000 ALTER TABLE `galeria_categorias` DISABLE KEYS */;
INSERT INTO `galeria_categorias` VALUES (1,0,'geral','geral','2016-09-02 17:58:19','2016-09-02 17:58:19');
/*!40000 ALTER TABLE `galeria_categorias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `galeria_imagens`
--

DROP TABLE IF EXISTS `galeria_imagens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galeria_imagens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `legenda` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `galeria_imagens`
--

LOCK TABLES `galeria_imagens` WRITE;
/*!40000 ALTER TABLE `galeria_imagens` DISABLE KEYS */;
INSERT INTO `galeria_imagens` VALUES (1,6,'exemplo_20160902175834.jpg','exemplo','2016-09-02 17:58:34','2016-09-02 17:58:34'),(2,5,'lorem-ipsum_20160902175841.png','lorem ipsum','2016-09-02 17:58:42','2016-09-02 17:58:42'),(3,4,'dolor-sit_20160902175850.jpeg','dolor sit','2016-09-02 17:58:51','2016-09-02 17:58:51'),(4,3,'pexels-photo-128639_20160902175856.jpeg','','2016-09-02 17:58:56','2016-09-02 17:58:56'),(5,2,'abstract-art-depths-of-emotion_20160902175935.jpg','','2016-09-02 17:59:36','2016-09-02 17:59:36'),(6,1,'sky-clouds-clouds-form-cumulus-clouds_20160902180001.jpg','','2016-09-02 18:00:02','2016-09-02 18:00:02');
/*!40000 ALTER TABLE `galeria_imagens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marcenaria_imagens`
--

DROP TABLE IF EXISTS `marcenaria_imagens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marcenaria_imagens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `legenda` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marcenaria_imagens`
--

LOCK TABLES `marcenaria_imagens` WRITE;
/*!40000 ALTER TABLE `marcenaria_imagens` DISABLE KEYS */;
INSERT INTO `marcenaria_imagens` VALUES (1,0,'abstract-art-depths-of-emotion_20160902180104.jpg','','2016-09-02 18:01:04','2016-09-02 18:01:04'),(2,0,'pexels-photo_20160902180110.jpg','','2016-09-02 18:01:10','2016-09-02 18:01:10'),(3,0,'pexels-photo-69040_20160902180116.png','','2016-09-02 18:01:17','2016-09-02 18:01:17'),(4,0,'pexels-photo-90911_20160902180126.jpeg','','2016-09-02 18:01:26','2016-09-02 18:01:26'),(5,0,'pexels-photo-128639_20160902180130.jpeg','','2016-09-02 18:01:31','2016-09-02 18:01:31'),(6,0,'sky-clouds-clouds-form-cumulus-clouds_20160902180135.jpg','','2016-09-02 18:01:35','2016-09-02 18:01:35');
/*!40000 ALTER TABLE `marcenaria_imagens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marcenaria_sob_medida`
--

DROP TABLE IF EXISTS `marcenaria_sob_medida`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marcenaria_sob_medida` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `links` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marcenaria_sob_medida`
--

LOCK TABLES `marcenaria_sob_medida` WRITE;
/*!40000 ALTER TABLE `marcenaria_sob_medida` DISABLE KEYS */;
INSERT INTO `marcenaria_sob_medida` VALUES (1,'<ul>\r\n	<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n	<li>Pellentesque a ipsum feugiat, ultricies nulla ut, euismod mauris.</li>\r\n	<li>Donec lobortis nulla eu justo vulputate, id ultricies arcu ullamcorper.</li>\r\n	<li>Nulla facilisi.</li>\r\n</ul>\r\n','&raquo; <a href=\"#\">M&Oacute;VEIS PLANEJADOS</a><br />\r\n&raquo; <a href=\"#\">M&Oacute;VEIS SOB MEDIDA</a><br />\r\n&raquo; <a href=\"#\">ARM&Aacute;RIOS DE COZINHA</a>',NULL,'2016-09-02 18:09:48');
/*!40000 ALTER TABLE `marcenaria_sob_medida` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2016_02_01_000000_create_users_table',1),('2016_03_01_000000_create_contato_table',1),('2016_03_01_000000_create_contatos_recebidos_table',1),('2016_08_30_142630_create_banners_table',1),('2016_08_30_143234_create_rodape_table',1),('2016_08_30_164619_create_marcenaria_sob_medida_table',1),('2016_08_30_175811_create_marcenaria_imagens_table',1),('2016_08_30_211041_create_galeria_tables',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rodape`
--

DROP TABLE IF EXISTS `rodape`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rodape` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `links` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rodape`
--

LOCK TABLES `rodape` WRITE;
/*!40000 ALTER TABLE `rodape` DISABLE KEYS */;
INSERT INTO `rodape` VALUES (1,'<a href=\"#\">m&oacute;veis sob medida para cozinhas</a><br />\r\n<a href=\"#\">m&oacute;veis sob medida para salas</a><br />\r\n<a href=\"#\">m&oacute;veis sob medida para quartos</a><br />\r\n<a href=\"#\">m&oacute;veis sob medida para banheiros</a><br />\r\n<a href=\"#\">m&oacute;veis planejados</a>',NULL,'2016-09-02 18:07:05');
/*!40000 ALTER TABLE `rodape` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'trupe','contato@trupe.net','$2y$10$J.ZpPxGwKszPv9UtyPRUvep8FUAQEfbit0RS3deYmLRBCfw0Q1zCS',NULL,NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-09-02 18:15:04
