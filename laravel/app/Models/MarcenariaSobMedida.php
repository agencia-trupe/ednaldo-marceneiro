<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class MarcenariaSobMedida extends Model
{
    protected $table = 'marcenaria_sob_medida';

    protected $guarded = ['id'];

}
