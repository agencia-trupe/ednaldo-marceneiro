<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class GaleriaImagem extends Model
{
    protected $table = 'galeria_imagens';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function getCategoriasListAttribute()
    {
        return $this->categorias->lists('id')->toArray();
    }

    public function categorias()
    {
        return $this->belongsToMany('App\Models\GaleriaCategoria', 'galeria_categoria_imagem', 'imagem_id', 'categoria_id')->ordenados();
    }

    public static function upload_imagem($legenda = null)
    {
        return CropImage::make('imagem', [
            [
                'width'  => 180,
                'height' => 180,
                'path'   => 'assets/img/galeria/thumbs/',
            ],
            [
                'width'  => 320,
                'height' => null,
                'path'   => 'assets/img/galeria/',
            ],
            [
                'width'  => 980,
                'height' => null,
                'path'   => 'assets/img/galeria/ampliacao/',
            ]
        ], $legenda);
    }
}
