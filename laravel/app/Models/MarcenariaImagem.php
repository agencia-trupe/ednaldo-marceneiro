<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class MarcenariaImagem extends Model
{
    protected $table = 'marcenaria_imagens';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_imagem($legenda = null)
    {
        return CropImage::make('imagem', [
            [
                'width'  => 300,
                'height' => 180,
                'path'   => 'assets/img/marcenaria-imagens/',
            ],
            [
                'width'  => 980,
                'height' => null,
                'path'   => 'assets/img/marcenaria-imagens/ampliacao/',
            ]
        ], $legenda);
    }

}
