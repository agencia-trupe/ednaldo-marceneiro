<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('marcenaria-sob-medida', 'MarcenariaController@index')->name('marcenaria');
    Route::get('galeria-de-fotos', 'GaleriaController@index')->name('galeria');
    Route::get('contato', 'ContatoController@index')->name('contato');
    Route::post('contato', 'ContatoController@envio')->name('contato.envio');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
        Route::resource('galeria/categorias', 'GaleriaCategoriasController');
        Route::resource('galeria', 'GaleriaImagensController');
		Route::resource('marcenaria-imagens', 'MarcenariaImagensController');
		Route::resource('marcenaria-sob-medida', 'MarcenariaSobMedidaController', ['only' => ['index', 'update']]);
		Route::resource('rodape', 'RodapeController', ['only' => ['index', 'update']]);
		Route::resource('banners', 'BannersController');
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::resource('usuarios', 'UsuariosController');

        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
