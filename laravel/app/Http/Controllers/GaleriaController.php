<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\GaleriaImagem;

class GaleriaController extends Controller
{
    public function index()
    {
        $imagens = GaleriaImagem::ordenados()->get();

        return view('frontend.galeria', compact('imagens'));
    }
}
