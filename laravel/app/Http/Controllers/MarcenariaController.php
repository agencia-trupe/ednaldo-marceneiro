<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\MarcenariaSobMedida;
use App\Models\MarcenariaImagem;

class MarcenariaController extends Controller
{
    public function index()
    {
        $marcenaria = MarcenariaSobMedida::first();
        $imagens = MarcenariaImagem::ordenados()->get();

        return view('frontend.marcenaria', compact('marcenaria', 'imagens'));
    }
}
