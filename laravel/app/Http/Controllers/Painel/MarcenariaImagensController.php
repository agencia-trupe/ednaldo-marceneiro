<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\MarcenariaImagensRequest;
use App\Http\Controllers\Controller;

use App\Models\MarcenariaImagem;

class MarcenariaImagensController extends Controller
{
    public function index()
    {
        $registros = MarcenariaImagem::ordenados()->get();

        return view('painel.marcenaria-imagens.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.marcenaria-imagens.create');
    }

    public function store(MarcenariaImagensRequest $request)
    {
        try {

            $input = $request->all();

            $input['imagem'] = MarcenariaImagem::upload_imagem($input['legenda']);

            MarcenariaImagem::create($input);
            return redirect()->route('painel.marcenaria-imagens.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(MarcenariaImagem $registro)
    {
        return view('painel.marcenaria-imagens.edit', compact('registro'));
    }

    public function update(MarcenariaImagensRequest $request, MarcenariaImagem $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = MarcenariaImagem::upload_imagem($input['legenda']);

            if ( ! isset($input['imagem']) && $input['legenda'] !== $registro->legenda) {
                $oldPath      = 'assets/img/marcenaria-imagens/ampliacao/'.$registro->imagem;
                $oldPathThumb = 'assets/img/marcenaria-imagens/'.$registro->imagem;

                $extension = \File::extension($oldPath);
                $fileName  = ($input['legenda']) ? str_slug($input['legenda']).'_'.date('YmdHis').'.'.$extension : date('YmdHis').'.'.$extension;

                $newPath      = 'assets/img/marcenaria-imagens/ampliacao/'.$fileName;
                $newPathThumb = 'assets/img/marcenaria-imagens/'.$fileName;

                if ( ! \File::move($oldPathThumb, $newPathThumb) || ! \File::move($oldPath, $newPath)) throw new Exception('Erro ao renomear arquivo de imagem', 1);

                $input['imagem'] = $fileName;
            }

            $registro->update($input);
            return redirect()->route('painel.marcenaria-imagens.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(MarcenariaImagem $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.marcenaria-imagens.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
