<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\GaleriaImagensRequest;

use App\Http\Controllers\Controller;

use App\Models\GaleriaCategoria;
use App\Models\GaleriaImagem;

class GaleriaImagensController extends Controller
{
    public function index()
    {
        $imagens = GaleriaImagem::ordenados()->get();

        return view('painel.galeria.index', compact('imagens'));
    }

    public function create()
    {
        $categorias = GaleriaCategoria::ordenados()->lists('titulo', 'id');

        return view('painel.galeria.create', compact('categorias'));
    }

    public function store(GaleriaImagensRequest $request)
    {
        try {

            $input = $request->except('categorias_list');

            $input['imagem'] = GaleriaImagem::upload_imagem($input['legenda']);

            $imagem = GaleriaImagem::create($input);
            $imagem->categorias()->sync($request->input('categorias_list'));

            return redirect()->route('painel.galeria.index')->with('success', 'Imagem adicionada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar imagem: '.$e->getMessage()]);

        }
    }

    public function edit(GaleriaImagem $imagem)
    {
        $categorias = GaleriaCategoria::ordenados()->lists('titulo', 'id');

        return view('painel.galeria.edit', compact('categorias', 'imagem'));
    }

    public function update(GaleriaImagensRequest $request, GaleriaImagem $imagem)
    {
        try {

            $input = array_filter($request->except('categorias_list'), 'strlen');

            if (isset($input['imagem'])) $input['imagem'] = GaleriaImagem::upload_imagem($input['legenda']);

            if ( ! isset($input['imagem']) && $input['legenda'] !== $imagem->legenda) {
                $oldPath            = 'assets/img/galeria/ampliacao/'.$imagem->imagem;
                $oldPathThumb       = 'assets/img/galeria/'.$imagem->imagem;
                $oldPathThumbPainel = 'assets/img/galeria/thumbs/'.$imagem->imagem;

                $extension = \File::extension($oldPath);
                $fileName  = ($input['legenda']) ? str_slug($input['legenda']).'_'.date('YmdHis').'.'.$extension : date('YmdHis').'.'.$extension;

                $newPath            = 'assets/img/galeria/ampliacao/'.$fileName;
                $newPathThumb       = 'assets/img/galeria/'.$fileName;
                $newPathThumbPainel = 'assets/img/galeria/thumbs/'.$fileName;

                if ( ! \File::move($oldPathThumb, $newPathThumb) || ! \File::move($oldPath, $newPath) || ! \File::move($oldPathThumbPainel, $newPathThumbPainel)) throw new Exception('Erro ao renomear arquivo de imagem', 1);

                $input['imagem'] = $fileName;
            }

            $imagem->update($input);
            $imagem->categorias()->sync($request->input('categorias_list'));

            return redirect()->route('painel.galeria.index')->with('success', 'Imagem alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar imagem: '.$e->getMessage()]);

        }
    }

    public function destroy(GaleriaImagem $imagem)
    {
        try {

            $imagem->delete();
            return redirect()->route('painel.galeria.index')
                             ->with('success', 'Imagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: '.$e->getMessage()]);

        }
    }
}
