<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\GaleriaCategoriasRequest;
use App\Http\Controllers\Controller;

use App\Models\GaleriaCategoria;
use App\Helpers\CropImage;

class GaleriaCategoriasController extends Controller
{
    public function index()
    {
        $categorias = GaleriaCategoria::ordenados()->get();

        return view('painel.galeria.categorias.index', compact('categorias'));
    }

    public function create()
    {
        return view('painel.galeria.categorias.create');
    }

    public function store(GaleriaCategoriasRequest $request)
    {
        try {

            $input = $request->all();

            GaleriaCategoria::create($input);
            return redirect()->route('painel.galeria.categorias.index')->with('success', 'Categoria adicionada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar categoria: '.$e->getMessage()]);

        }
    }

    public function edit(GaleriaCategoria $categoria)
    {
        return view('painel.galeria.categorias.edit', compact('categoria'));
    }

    public function update(GaleriaCategoriasRequest $request, GaleriaCategoria $categoria)
    {
        try {

            $input = array_filter($request->all(), 'strlen');

            $categoria->update($input);
            return redirect()->route('painel.galeria.categorias.index')->with('success', 'Categoria alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar categoria: '.$e->getMessage()]);

        }
    }

    public function destroy(GaleriaCategoria $categoria)
    {
        try {

            $categoria->delete();
            return redirect()->route('painel.galeria.categorias.index')->with('success', 'Categoria excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir categoria: '.$e->getMessage()]);

        }
    }
}
