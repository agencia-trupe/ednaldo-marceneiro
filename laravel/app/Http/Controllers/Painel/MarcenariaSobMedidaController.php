<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\MarcenariaSobMedidaRequest;
use App\Http\Controllers\Controller;

use App\Models\MarcenariaSobMedida;

class MarcenariaSobMedidaController extends Controller
{
    public function index()
    {
        $registro = MarcenariaSobMedida::first();

        return view('painel.marcenaria-sob-medida.edit', compact('registro'));
    }

    public function update(MarcenariaSobMedidaRequest $request, MarcenariaSobMedida $registro)
    {
        try {
            $input = $request->all();


            $registro->update($input);

            return redirect()->route('painel.marcenaria-sob-medida.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
