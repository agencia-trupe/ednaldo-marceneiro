<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('frontend.common.footer', function($view) {
            $view->with('rodape', \App\Models\Rodape::first());
        });
        view()->composer(['frontend.common.header', 'frontend.common.footer'], function($view) {
            $view->with('contato', \App\Models\Contato::first());
        });
        view()->composer('painel.common.nav', function($view) {
            $view->with('contatosNaoLidos', \App\Models\ContatoRecebido::naoLidos()->count());
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
