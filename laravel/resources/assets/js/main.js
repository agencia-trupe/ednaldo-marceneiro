(function(window, document, $, undefined) {
    'use strict';

    var App = {};

    App.mobileToggle = function() {
        var $handle = $('#mobile-toggle'),
            $nav    = $('#nav-mobile');

        $handle.on('click touchstart', function(event) {
            event.preventDefault();
            $nav.slideToggle();
            $handle.toggleClass('close');
        });
    };

    App.bannersHome = function() {
        var $wrapper = $('.banners');
        if (!$wrapper.length) return;

        $wrapper.cycle({
            slides: '>.banner',
            pagerTemplate: '<a href="#">{{slideNum}}</a>',
            autoHeight: 'calc'
        });
    };

    App.galeriaMasonry = function() {
        var $grid = $('.galeria-masonry');
        if (!$grid.length) return;

        var $initialItems = $grid.find('.galeria-thumb').hide();

        $grid.masonry({
            itemSelector: 'no-items',
            columnWidth: '.grid-sizer',
            gutter: '.gutter-sizer'
        })
        .masonry( 'option', { itemSelector: '.galeria-thumb' })
        .masonryImagesReveal($initialItems);
    };

    App.galeriaLightbox = function() {
        $('.fancybox').fancybox({
            padding: 10,
            helpers: {
                title: { type: 'inside' }
            }
        });
    };

    App.envioContato = function(event) {
        event.preventDefault();

        var $form     = $(this),
            $response = $('#form-contato-response');

        $response.fadeOut('fast');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/contato',
            data: {
                nome: $('#nome').val(),
                email: $('#email').val(),
                telefone: $('#telefone').val(),
                mensagem: $('#mensagem').val(),
            },
            success: function(data) {
                $response.fadeOut().text(data.message).fadeIn('slow');
                $form[0].reset();
            },
            error: function(data) {
                $response.fadeOut().text('Preencha todos os campos corretamente').fadeIn('slow');
            },
            dataType: 'json'
        });
    };

    App.init = function() {
        this.mobileToggle();
        this.bannersHome();
        this.galeriaMasonry();
        this.galeriaLightbox();
        $('#form-contato').on('submit', this.envioContato);
    };

    $(document).ready(function() {
        $.fn.masonryImagesReveal = function( $items ) {
          var msnry = this.data('masonry');
          var itemSelector = msnry.options.itemSelector;
          // hide by default
          $items.hide();
          // append to container
          this.append( $items );
          $items.imagesLoaded( function() {
            // un-hide items
            $items.show();
            // reveal all of them
            msnry.appended( $items );
          });

          return this;
        };

        App.init();


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

}(window, document, jQuery));
