@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Galeria /</small> Adicionar Categoria</h2>
    </legend>

    {!! Form::open(['route' => 'painel.galeria.categorias.store']) !!}

        @include('painel.galeria.categorias.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
