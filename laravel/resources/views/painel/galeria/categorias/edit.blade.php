@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Galeria /</small> Editar Categoria</h2>
    </legend>

    {!! Form::model($categoria, [
        'route'  => ['painel.galeria.categorias.update', $categoria->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.galeria.categorias.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
