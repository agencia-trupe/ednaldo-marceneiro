@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Galeria /</small> Adicionar Imagem</h2>
    </legend>

    {!! Form::open(['route' => 'painel.galeria.store', 'files' => true]) !!}

        @include('painel.galeria.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
