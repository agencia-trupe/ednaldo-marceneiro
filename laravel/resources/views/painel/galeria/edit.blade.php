@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Galeria /</small> Editar Imagem</h2>
    </legend>

    {!! Form::model($imagem, [
        'route'  => ['painel.galeria.update', $imagem->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.galeria.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
