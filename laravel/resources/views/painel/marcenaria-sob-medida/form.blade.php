@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('texto', 'Texto') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'marcenaria']) !!}
</div>

<div class="form-group">
    {!! Form::label('links', 'Links') !!}
    {!! Form::textarea('links', null, ['class' => 'form-control ckeditor', 'data-editor' => 'links']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
