@extends('painel.common.template')

@section('content')

    <legend>
        <h2>
            Marcenaria sob medida
            <a href="{{ route('painel.marcenaria-imagens.index') }}" class="btn btn-info btn-sm pull-right"><span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>Editar Imagens</a>
        </h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.marcenaria-sob-medida.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.marcenaria-sob-medida.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
