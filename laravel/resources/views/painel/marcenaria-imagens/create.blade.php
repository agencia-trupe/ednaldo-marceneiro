@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Marcenaria / Imagens /</small> Adicionar Imagem</h2>
    </legend>

    {!! Form::open(['route' => 'painel.marcenaria-imagens.store', 'files' => true]) !!}

        @include('painel.marcenaria-imagens.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
