@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Marcenaria / Imagens /</small> Editar Imagem</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.marcenaria-imagens.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.marcenaria-imagens.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
