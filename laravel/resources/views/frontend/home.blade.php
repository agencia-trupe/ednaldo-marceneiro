@extends('frontend.common.template')

@section('content')

    <div class="home">
        <div class="banners">
            @foreach($banners as $banner)
            <div class="banner">
                <div class="center">
                    <div class="imagem">
                        <img src="{{ asset('assets/img/banners/'.$banner->imagem) }}" alt="">
                    </div>
                    <div class="texto">
                        <h3>{{ $banner->subtitulo }}</h3>
                        <h2>{{ $banner->titulo }}</h2>
                        <p>{{ $banner->frase }}</p>
                        <a href="{{ route('contato') }}">Peça seu orçamento &raquo;</a>
                    </div>
                </div>
            </div>
            @endforeach

            <div class="cycle-pager"></div>
        </div>

        <div class="chamadas">
            <div class="center">
                <div class="chamada chamada-1">
                    <p>Executamos o projeto de marcenaria sob medida que você imaginar!</p>
                </div>
                <div class="chamada chamada-2">
                    <p>Temos parceria com empresa de pedras - granitos, pedras sintéticas e mármores</p>
                </div>
                <div class="chamada chamada-3">
                    <p>Trabalhamos<br> com os melhores materiais do mercado. MDF/MDP, Fórmica e amadeirados.</p>
                </div>
                <div class="chamada chamada-4">
                    <p>Fazemos orçamento sem compromisso - solicite já o seu</p>
                </div>
            </div>
        </div>

        @include('frontend.common.chamada-orcamento')
    </div>

@endsection
