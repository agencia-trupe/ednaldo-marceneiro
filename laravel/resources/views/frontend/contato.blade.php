@extends('frontend.common.template')

@section('content')

    <div class="contato">
        <div class="center">
            <div class="informacoes">
                <h2>FALE CONOSCO<br>PEÇA SEU ORÇAMENTO</h2>
                <p class="telefone">{{ $contato->telefone }} <span>({{ $contato->telefone_operadora }})</span></p>
                <p class="whatsapp">{{ $contato->whatsapp }} <span>({{ $contato->whatsapp_operadora }})</span></p>
            </div>

            <form action="" id="form-contato" method="POST">
                <input type="text" name="nome" id="nome" placeholder="nome" required>
                <input type="email" name="email" id="email" placeholder="e-mail" required>
                <input type="text" name="telefone" id="telefone" placeholder="telefone">
                <textarea name="mensagem" id="mensagem" placeholder="mensagem" required></textarea>
                <div id="form-contato-response"></div>
                <input type="submit" value="ENVIAR">
            </form>
        </div>
    </div>

@endsection
