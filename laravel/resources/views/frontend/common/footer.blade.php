    <footer>
        <div class="center">
            <div class="wrapper">
                <div class="informacoes">
                    <div class="col">
                        <a href="{{ route('home') }}">home</a>
                        <a href="{{ route('marcenaria') }}">marcenaria sob medida</a>
                        <a href="{{ route('galeria') }}">galeria de fotos</a>
                        <a href="{{ route('contato') }}">contato</a>
                    </div>

                    <div class="col">
                        <p>{!! $rodape->links !!}</p>
                    </div>

                    <div class="col">
                        <p class="telefone">{{ $contato->telefone }} <span>({{ $contato->telefone_operadora }})</span></p><br>
                        <p class="whatsapp">{{ $contato->whatsapp }} <span>({{ $contato->whatsapp_operadora }})</span></p>
                    </div>
                </div>
            </div>
        </div>

        <div class="copyright">
            <p>
                © {{ date('Y') }} {{ config('site.name') }} - Todos os direitos reservados
                <span>-</span>
                <a href="http://www.trupe.net" target="_blank">Criação de sites</a>:
                <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
            </p>
        </div>
    </footer>
