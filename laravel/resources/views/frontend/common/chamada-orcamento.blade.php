<div class="chamada-orcamento">
    <div class="center">
        <div class="wrapper">
            <div class="left">
                <p>O seu móvel sob medida com marcenaria impecável, excelente atendimento, bom preço e orçamento sem complicação. Faça sua cotação conosco!</p>
            </div>
            <div class="right">
                <a href="{{ route('contato') }}">Peça seu orçamento &raquo;</a>
            </div>
        </div>
    </div>
</div>
