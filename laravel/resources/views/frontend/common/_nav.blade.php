<a href="{{ route('home') }}" @if(Route::currentRouteName() === 'home') class="active" @endif>
    home
</a>
<a href="{{ route('marcenaria') }}" @if(Route::currentRouteName() === 'marcenaria') class="active" @endif>
    marcenaria sob medida
</a>
<a href="{{ route('galeria') }}" @if(Route::currentRouteName() === 'galeria') class="active" @endif">
    galeria de fotos
</a>
<a href="{{ route('contato') }}" @if(Route::currentRouteName() === 'contato') class="active" @endif">
    contato
</a>
