@extends('frontend.common.template')

@section('content')

    <div class="galeria">
        <div class="center">
            <div class="galeria-masonry">
                <div class="gutter-sizer"></div>
                <div class="grid-sizer"></div>
                @foreach($imagens as $imagem)
                <a href="{{ asset('assets/img/galeria/ampliacao/'.$imagem->imagem) }}" class="galeria-thumb fancybox" title="{{ $imagem->legenda }}" rel="galeria">
                    <img src="{{ asset('assets/img/galeria/'.$imagem->imagem) }}" alt="">
                    <div class="overlay"></div>
                </a>
                @endforeach
            </div>
        </div>

        @include('frontend.common.chamada-orcamento')
    </div>

@endsection
