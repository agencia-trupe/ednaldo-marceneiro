@extends('frontend.common.template')

@section('content')

    <div class="marcenaria">
        <div class="marcenaria-chamada">
            <div class="center">
                <div class="chamada">
                    <h1>Marcenaria sob medida</h1>
                    <div class="texto">
                        {!! $marcenaria->texto !!}
                    </div>
                    <a href="{{ route('contato') }}">Peça seu orçamento &raquo;</a>
                </div>
                <div class="links">
                    {!! $marcenaria->links !!}
                </div>
            </div>
        </div>
        <div class="marcenaria-trabalhos">
            <div class="center">
                <h3>Alguns trabalhos desenvolvidos:</h3>
                <div class="marcenaria-imagens">
                    @foreach($imagens as $imagem)
                    <a href="{{ asset('assets/img/marcenaria-imagens/ampliacao/'.$imagem->imagem) }}" class="fancybox" title="{{ $imagem->legenda }}" rel="galeria">
                        <img src="{{ asset('assets/img/marcenaria-imagens/'.$imagem->imagem) }}" alt="">
                        <div class="overlay"></div>
                    </a>
                    @endforeach
                </div>
                <a href="{{ route('galeria') }}" class="marcenaria-galeria">
                    Veja mais em nossa galeria de fotos &raquo;
                </a>
            </div>
        </div>

        @include('frontend.common.chamada-orcamento')
    </div>

@endsection
