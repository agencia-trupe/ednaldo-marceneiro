<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarcenariaSobMedidaTable extends Migration
{
    public function up()
    {
        Schema::create('marcenaria_sob_medida', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto');
            $table->text('links');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('marcenaria_sob_medida');
    }
}
