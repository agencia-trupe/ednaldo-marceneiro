<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRodapeTable extends Migration
{
    public function up()
    {
        Schema::create('rodape', function (Blueprint $table) {
            $table->increments('id');
            $table->text('links');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('rodape');
    }
}
