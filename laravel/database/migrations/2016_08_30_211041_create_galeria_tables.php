<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGaleriaTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('galeria_categorias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->string('slug');
            $table->timestamps();
        });

        Schema::create('galeria_imagens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->text('legenda');
            $table->timestamps();
        });

        Schema::create('galeria_categoria_imagem', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('categoria_id')->unsigned()->index();
            $table->integer('imagem_id')->unsigned()->index();
            $table->timestamps();
            $table->foreign('categoria_id')->references('id')->on('galeria_categorias')->onDelete('cascade');
            $table->foreign('imagem_id')->references('id')->on('galeria_imagens')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('galeria_categoria_imagem');
        Schema::drop('galeria_imagens');
        Schema::drop('galeria_categorias');
    }
}
